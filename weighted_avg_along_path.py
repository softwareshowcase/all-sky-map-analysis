import numpy as np
from astropy.io import fits
from astropy.wcs import WCS
import matplotlib.pyplot as plt
import math
import timeit


class counts_map:
    def __init__(self, filename, path=None):
        ''' FITS is opened. Here you can set the FWHM and the cutoff for the ROI around
            each coordinate. '''
        if path == None:
            self.path = path = "/home/user/"
        else:
            self.path = path
        self.filename = filename
        self.hdu_list = fits.open(path+filename) # Opens the fits file
        self.header = fits.getheader(path+filename) # Grabs the header
        self.w = WCS(self.header, naxis=(1,2)) # creates WCS object for pix2wrld conversion 
        self.hdu = self.hdu_list[0]     # Primary data object for the fits file is stored
        self.FWHM = 6.5                 # full width at half maximum of instrument
        self.sigma = self.FWHM / 2.355  # calculate sigma
        self.cutoff = 3 * self.sigma    # cutoff for ROI around each coordinate of the path
        self.range_x = self.header['NAXIS1']
        if np.shape(self.hdu.data) == (1,91,181):
            non_nan_element_count = np.count_nonzero(~np.isnan(self.hdu.data[0]))
            print("# of pixels in map:", non_nan_element_count)
        else:
            non_nan_element_count = np.count_nonzero(~np.isnan(self.hdu.data))
            print("# of pixels in map:", non_nan_element_count)
        print(self.cutoff)
        print("File %s loaded." % filename)

    def read_coordinates(self):
        '''Opens the dat file holding time vs sky position and returns
            a list of pixel values corresponding to those coordinates based
            on the WCS object created from the FITS file'''
        coordinatefile = "coordinates.dat"
        x_lim = self.header['NAXIS1']
        y_lim = self.header['NAXIS2']
        with open(coordinatefile,"r") as f: # Opens up the coordinate file (read mode)
            x,y = [], []             # arrays will hold the pixel values of longitude,latitude
            for l in f:              # loop through each line (l) in the file (f)
                row = l.split()          # Each row has every column, so this 'splits' them up
                if row[1] == 'ldeg':           # This line just skips the column labels
                    continue
                time = float(row[0])           # time at current row
                if time < 335 or time > 373.5:
                # if time < 117 or time > 322:   # Defines a window of time consider
                # if time < 336.7:
                # if time < 321:
                # if time < 117 or time > 275.7:
                    continue
                l = float(row[1])           # defines l = longitude and b = latitude for the row
                b = float(row[2])
                long2pix, lat2pix =  self.w.wcs_world2pix(l,b, 1) # convert the l and b to a pix
                long2pix = np.int(np.round(long2pix)) # rounding and converting to integer
                lat2pix = np.int(np.round(lat2pix))
                if long2pix > x_lim or lat2pix > y_lim: # conversion sometimes returns pixels
                    continue                            # that are not in the map. (extrapolates)
                if long2pix < 1 or lat2pix < 1:         # here we exclude those points
                    continue
                x.append(long2pix)                      # store the values
                y.append(lat2pix)
        xylist=[]                                       # will hold all of the coordinate pairs
        for number in range(len(x)):          # Stores every unique pixel pair (x,y) in xy_list
            if ([x[number],y[number]] not in xylist):
                xylist.append([x[number],y[number]])
        print("Coordinates along flight path stored")
        return xylist

    def plot_path(self):
        xylist = self.read_coordinates()
        for pair in xylist:
            plt.plot(pair[0], pair[1], 'b.', markersize=0.5)
            # print(pair[0],pair[1])
        plt.plot(0,0)
        plt.plot(1800,900)
        plt.show()

    def plot_CR_v_long(self):
        '''For use with a processed counts maps that has a weighted average along a flight path.
            Plots count rate vs longitude'''
        f= open("count_rate_v_long.txt","w+")
        for i in range(len(self.hdu.data)):
            for j in range(len(self.hdu.data[i])):
                if not np.isnan(self.hdu.data[i][j]):
                    x = j + 1
                    y = i + 1
                    count_rate = self.hdu.data[i][j]
                    longitude, latitude = self.w.wcs_pix2world(x, y, 1)
                    f.write("%f\t%f\n" % (longitude, count_rate))
                    # plt.plot(longitude, count_rate, 'r.', markersize = 1)
        # plt.show()
        f.close()

    def create_blank_FITS(self):
        ''' Creates blank array for FITS file where the weighted average at each point is stored '''
        hduarray=[]                      # holds values along path from imported FITS files
        range_x = self.header['NAXIS1']  # Get values for entire rectangular area of map
        range_y = self.header['NAXIS2']
        for coordinatey in range(range_y): # amount of rows in map is the height
            hduarray.append([])            # append an empty array for each row 
            for coordinatex in range(range_x): # The number of samples per row (columns) is given 
                hduarray[coordinatey].append(float('nan')) # by the width of the rectangular areas.
        print("Blank FITS array file created")
        return hduarray                            # We append a NaN to each row for every x pixel.

    def angular_distance(self, l0, b0, l, b):
        # cosine of the distance between the points
        cos_d = np.cos(np.pi/2 - b0) * np.cos(np.pi/2 - b) + np.sin(np.pi/2 - b0) * np.sin(np.pi/2 - b) * np.cos(l0 - l)
        d = np.arccos(cos_d) # invert to get the distance
        d = np.rad2deg(d)    # convert back to degrees for return
        return d

    def weight( self, d, sigma ):
        weight = np.exp(-d**2/sigma**2) # weight assigned to each term of the sum
        return weight
        
    def pix2gal_coord_radians(self, pix_x, pix_y):
        ''' Takes, as argument, an x and y pixel in the image and converts it to longitude
            and latitude. The coordinates are then converted to radians for use in the
            angular_distance function. '''
        longitude, latitude = self.w.wcs_pix2world(pix_x, pix_y, 1)
        longtitude = np.deg2rad(longitude)
        latitude = np.deg2rad(latitude)
        return longtitude, latitude

    def weighted_circular_region(self, center_coords, coords):
        ''' This is the bottle neck. For a particular central pixel, this function scans through
            all of the pixels in the image to find out if they are closer than the cutoff
            distance. If it's within the cutoff distance, it is assigned a weight. The pixel 
            coordinate pair and the weight are appended to an array comprising the ROI. '''
        x0 = center_coords[0]                       # x pixel coordinate of path point
        y0 = center_coords[1]                       # y pixel coordinate of path point
        l0, b0 = self.pix2gal_coord_radians(x0,y0)  # convert pixel pair to (l,b)
        ROI_coordinates = []
        for pair in coords:
            x = pair[0]                             # x pixel coordinate of test point
            y = pair[1]                             # y pixel coordinate of test point
            l, b = self.pix2gal_coord_radians(x, y) # convert pixel pair to (l,b)
            d = self.angular_distance(l0, b0, l, b) # calculate distance between the 2 points
            if d <= self.cutoff:                    # Checks if the distance is within the cutoff
                wgt = self.weight(d, self.sigma)    # weight is assigned
                ROI_coordinates.append((pair, wgt)) # pixel pair is added to the ROI list along with wgt
        return ROI_coordinates

    def weighted_average(self, data, ROI_coordinates):
        '''Calculates weighted average of the ROI and returns it'''
        sum_of_wgted_vals = 0
        sum_of_weights = 0
        for point in ROI_coordinates:
            coordinates = point[0] # coordinate pair defined
            weight = point[1]      # weight defined
            x = coordinates[0] - 1 # NOTE: Index discrepancy
            y = coordinates[1] - 1 # FITS 1 indexed python 0 indexed
            value = data[y][x]     # gets count rate from FITS file at the point in question
            # PAY CLOSE ATTENTION HERE: Depending on how blank values are 
            # handled in your original fits file, you have to make sure
            # you handle them accordingly
            if value <= 0:
                continue
            # if np.isnan(value):
            #     # print("ROI point has nan")
            #     continue
            sum_of_wgted_vals += value*weight # add term to total sum of weighted count rates
            sum_of_weights += weight          # add term to total sum of weights
        try:
            wgted_avg = sum_of_wgted_vals/sum_of_weights
            if np.isnan(wgted_avg):
                print('weighted average:', wgted_avg)
        except ZeroDivisionError:
            print("Attempted to divide by zero")
            print("Sum of weighted values:", sum_of_wgted_vals)
            print("Sum of weights:", sum_of_weights)
            wgted_avg = float('nan')
        return wgted_avg

    def two_piece_slice(self, pix2long, upper_lat, lower_lat, top):
        # Slice ROI into two boxes. Box 1 on the left-side of the map and Box 2 on the right.
        if top:
            upper_x_1, dummy_y = self.w.wcs_world2pix(pix2long-self.cutoff*1.1,upper_lat,1)
            lower_x_1, dummy_y = self.w.wcs_world2pix(180, 0, 1)
            upper_x_2, dummy_y = self.w.wcs_world2pix(180.00001,0,1)
            lower_x_2, dummy_y = self.w.wcs_world2pix(pix2long+self.cutoff*1.1,upper_lat,1)
        else:
            upper_x_1, dummy_y = self.w.wcs_world2pix(pix2long-self.cutoff*1.1,lower_lat,1)
            lower_x_1, dummy_y = self.w.wcs_world2pix(180, 0, 1)
            upper_x_2, dummy_y = self.w.wcs_world2pix(180.00001,0,1)
            lower_x_2, dummy_y = self.w.wcs_world2pix(pix2long+self.cutoff*1.1,lower_lat,1)
        upper_x_1 = np.int(np.round(upper_x_1))
        lower_x_1 = np.int(np.round(lower_x_1))
        upper_x_2 = np.int(np.round(upper_x_2))
        lower_x_2 = np.int(np.round(lower_x_2))
        return upper_x_1, lower_x_1, upper_x_2, lower_x_2

    def slice_map(self, pix2long, pix2lat):
        coords = []
        # Assumes center is (0,0). This block determines the quadrant of the ROI center
        if pix2long > 0 and pix2long < 180:
            left = True
        else:
            left = False
        if pix2lat > 0:
            top = True
        else:
            top = False
        # print(pix2long, pix2lat)
        # The upper and lower latitude for the scan can be defined
        # for any point, since it is a real angle. +/- the cutoff
        upper_lat = pix2lat + self.cutoff*1.1
        lower_lat = pix2lat - self.cutoff*1.1
        # We only need the upper and lower y pixel (unused dummy_x)
        dummy_x, y_upper = self.w.wcs_world2pix(pix2long, upper_lat, 1)
        dummy_x, y_lower = self.w.wcs_world2pix(pix2long, lower_lat, 1)
        # Make the pixel value an integer
        y_lower = np.int(np.round(y_lower))
        y_upper = np.int(np.round(y_upper))
        # Assume we will not slice horizontally along entire map (at first)
        slice_in_two = False
        # NOTE: Longitude increases to the left in these maps
        # The quadrant the ROI center is in determines the deformation of the
        # circle on the map. For first case, top of circle points toward central
        # longitude and the bottom points toward map edge
        if left and top:
            inner_long = pix2long - self.cutoff*1.1
            outer_long = pix2long + self.cutoff*1.1
            if not (outer_long < 180 and pix2long < 180):
                slice_in_two = True
                upper_x_1, lower_x_1, upper_x_2, lower_x_2 = self.two_piece_slice(pix2long, upper_lat, lower_lat, top)
            else:
                x_upper, dummy_y = self.w.wcs_world2pix(inner_long, upper_lat, 1)
                x_lower, dummy_y = self.w.wcs_world2pix(outer_long, lower_lat, 1)
        if not left and top:
            inner_long = pix2long + self.cutoff*1.1
            outer_long = pix2long - self.cutoff*1.1
            if not (outer_long > 180 and pix2long > 180):
                slice_in_two = True
                upper_x_1, lower_x_1, upper_x_2, lower_x_2 = self.two_piece_slice(pix2long, upper_lat, lower_lat, top)
            else:
                x_lower, dummy_y = self.w.wcs_world2pix(inner_long, upper_lat, 1)
                x_upper, dummy_y = self.w.wcs_world2pix(outer_long, lower_lat, 1)
        if left and not top:
            inner_long = pix2long - self.cutoff*1.1
            outer_long = pix2long + self.cutoff*1.1
            if not (outer_long < 180 and pix2long < 180):
                slice_in_two = True
                upper_x_1, lower_x_1, upper_x_2, lower_x_2 = self.two_piece_slice(pix2long, upper_lat, lower_lat, top)
            else:
                x_lower, dummy_y = self.w.wcs_world2pix(outer_long, upper_lat, 1)
                x_upper, dummy_y = self.w.wcs_world2pix(inner_long, lower_lat, 1)
        if not left and not top:
            inner_long = pix2long + self.cutoff*1.1
            outer_long = pix2long - self.cutoff*1.1
            if not (outer_long > 180 and pix2long > 180):
                slice_in_two = True
                upper_x_1, lower_x_1, upper_x_2, lower_x_2 = self.two_piece_slice(pix2long, upper_lat, lower_lat, top)
            else:
                x_lower, dummy_y = self.w.wcs_world2pix(inner_long, lower_lat, 1)
                x_upper, dummy_y = self.w.wcs_world2pix(outer_long, upper_lat, 1)
        # After checking POI quadrant the POI longitude and the outermost longitude
        # are checked against the edge of the map. If both are within the edge there
        # is no need to scan the entire horizontal slice.
        if slice_in_two:
            for i in range(lower_x_1, upper_x_1+1):
                for j in range(y_lower,y_upper+1):
                    coords.append((i,j))
            for i in range(lower_x_2, upper_x_2+1):
                for j in range(y_lower,y_upper+1):
                    coords.append((i,j))
        else:
            x_lower = np.int(np.round(x_lower))
            x_upper = np.int(np.round(x_upper))
            for i in range(x_lower, x_upper+1):
                for j in range(y_lower,y_upper+1):
                    coords.append((i,j))
        return coords

    def write_weighted_avg_along_path(self): 
        hduarray = self.create_blank_FITS() # creates the blank fits array to store the new data in
        range_x = self.header['NAXIS1']     # defines the amount of x pixels 
        range_y = self.header['NAXIS2']     # defines the amount of y pixels
        xylist = self.read_coordinates()        # defines pixel coordinates for flight path
        if np.shape(self.hdu.data) == (1,91,181):   # some files have different shape
            for center_coords in xylist:            # loop through the flight path coordinate pairs
                start_time = timeit.default_timer() # start time for a timer
                x = center_coords[0]                # x coordinate of the flight path point
                y = center_coords[1]                # y coordinate of the flight path point
                pix2long, pix2lat = self.w.wcs_pix2world(x, y, 1)
                if math.isnan(pix2long) or math.isnan(pix2lat):
                    continue
                coords = self.slice_map(pix2long, pix2lat)
                ROI_coordinates = self.weighted_circular_region(center_coords, coords)
                data_point = self.weighted_average(self.hdu.data[0], ROI_coordinates)
                if math.isnan(data_point):          # for undefined data points we skip to the next coordinate
                    continue
                hduarray[y-1][x-1]= data_point              # NOTE: indexing discrepancy
                # print("Elapsed time:", timeit.default_timer() - start_time, "\nData point:", data_point)
            self.header.remove('BSCALE')
            self.header.remove('BZERO')
        else:                                       # assume shape of non-wisc. files
            for center_coords in xylist:            # loop through the flight path coordinate pairs
                start_time = timeit.default_timer() # start time for a timer
                x = center_coords[0]                # x coordinate of the flight path point
                y = center_coords[1]                # y coordinate of the flight path point
                pix2long, pix2lat = self.w.wcs_pix2world(x, y, 1)
                if math.isnan(pix2long) or math.isnan(pix2lat):
                    continue
                coords = self.slice_map(pix2long, pix2lat)
                ROI_coordinates = self.weighted_circular_region(center_coords, coords)
                data_point = self.weighted_average(self.hdu.data, ROI_coordinates)
                if math.isnan(data_point):          # for undefined data points we skip to the next coordinate
                    continue
                hduarray[y-1][x-1]= data_point              # NOTE: indexing discrepancy
                print(timeit.default_timer() - start_time)  # Time elapsed for this code block is printed
        newhdu= fits.PrimaryHDU(hduarray)               # Next lines create the new FITS file
        newhdu.header = self.header
        newhdu.header['DATAMAX'] = np.nanmax(hduarray)
        newhdu.header['DATAMIN'] = np.nanmin(hduarray)
        newhdu.writeto('./weightedAverageAlongPath_%s' % self.filename,  overwrite=True)
        self.hdu_list.close()                           # close FITS file

if __name__ == '__main__':
    skySurvey = counts_map('survey.fits')
    skySurvey.write_weighted_avg_along_path()