import numpy as np
from astropy.io import fits
from astropy.wcs import WCS
import matplotlib.pyplot as plt
import math
import timeit


class counts_maps:
    def __init__(self, *argv, path=None):
        ''' Can take one or more counts maps FITS files to perform manipulations '''
        if path == None:
            self.path = path = "/home/user/"
        else:
            self.path = path
        self.filenames = []
        self.hdu_lists = []
        self.headers = []
        self.ws = []
        self.hdus = []
        self.range_xs = []
        for i in range(len(argv)):
            self.filenames.append(argv[i])
            self.hdu_lists.append(fits.open(path+argv[i]))
            self.headers.append(fits.getheader(path+argv[i]))
            self.ws.append(WCS(self.headers[i], naxis=(1,2)))
            self.hdus.append(self.hdu_lists[i][0])
            self.range_xs.append(self.headers[i]['NAXIS1'])
            print("File %s loaded." % self.filenames[i])
    
    def print_data_column(self):
        '''Print data from the second catalog of x-ray sources.'''
        with open("cat2rxs.dat", 'r') as catalog:
            i = 0
            for line in catalog:
                source_extent = float(line[176:186])
                if source_extent > 1:
                    # print(source_extent)
                    i += 1
                # if i == 10:
                #     break

    def sum_maps(self):
        sum_of_maps = self.hdus[0].data
        for i in range(len(self.hdus)-1):
            sum_of_maps += self.hdus[i+1].data
        return sum_of_maps
    
    def zero2NaN(self):
        # Take a map that has zero values and covert it to one with NaNs instead.
        hduarray = []
        range_x = self.headers[0]['NAXIS1']
        range_y = self.headers[0]['NAXIS2']
        for yCoord in range(range_y):
            hduarray.append([])
            for xCoord in range(range_x):
                dataPoint = self.hdus[0].data[yCoords][xCoord]
                if dataPoint <= 0:                    
                    hduarray[yCoord].append(float('nan'))
                else:
                    hduarray[yCoord].append(dataPoint)
        # Now write to a new file
        newhdu = fits.PrimaryHDU(hduarray)
        newhdu.header = self.headers[0]
        newhdu.header['DATAMAX'] = np.nanmax(hduarray)
        newhdu.header['DATAMIN'] = np.nanmin(hduarray)
        newhdu.writeto('./scanpath_FITS/noZeroesTest.fits',  overwrite=True)

    def write_sum_to_file(self):
        hduarray = self.sum_maps()
        newhdu= fits.PrimaryHDU(hduarray)
        newhdu.header = self.headers[0]
        combo_string = "type1"
        newhdu.header['OBJECT'] = "survey" + combo_string
        newhdu.header['DATAMAX'] = np.nanmax(hduarray)
        newhdu.header['DATAMIN'] = np.nanmin(hduarray)
        newhdu.writeto('./sum.fits',  overwrite=True)
        for hdu_list in self.hdu_lists:
            hdu_list.close()
    
    def create_blank_map(self):
        '''Choose a template map to make all of the elements within
            the projection zero.'''
        data_array = self.hdus[0].data
        for i in range(len(data_array)):
            for j in range(len(data_array[i])):
                if ~np.isnan(data_array[i][j]):
                    data_array[i][j] = 0
        newhdu= fits.PrimaryHDU(data_array)
        newhdu.header = self.headers[0]
        newhdu.header.remove('DATAMAX')
        newhdu.header.remove('DATAMIN')
        newhdu.header['OBJECT'] = "Blank"
        newhdu.writeto('./BLANK_skyview.fits',  overwrite=True)

    def write_point_sources_to_map(self):
        '''Takes a single BLANK fits file and writes to it the point source
            count rates in Snowdens (10^-6 counts/s/arcmin^2)'''
        with open("cat2rxs.dat", 'r') as catalog:
            for line in catalog:
                # See readme on 2XRS to find byte values for other data
                # print("IAU name:", line[0:22])
                glon = float(line[136:146])
                glat = float(line[146:156])
                exposure = float(line[81:90]) # in seconds
                try:
                    counts_A = float(line[915:924])
                    # print("Source counts in band B:\t", line[924:933])
                    # print("Source counts in band C:\t", line[933:942])
                    # print("Source counts in band D:\t", line[942:951])
                except ValueError:
                    continue
                count_rate_A = counts_A/exposure
                # if count_rate_A > 0.025:
                #     continue
                snowdens_A = count_rate_A/144      # dividing by 144 arcminutes^2
                snowdens_A *= 10**6                # in units of 10^-6 counts
                x, y = self.ws[0].wcs_world2pix(glon,glat,1)
                x -= 1
                y -= 1
                x = np.int(np.round(x)) 
                y = np.int(np.round(y))
                self.hdus[0].data[y][x] = snowdens_A
        newhdu= fits.PrimaryHDU(self.hdus[0].data)
        newhdu.header = self.headers[0]
        newhdu.header['DATAMAX'] = np.nanmax(self.hdus[0].data)
        newhdu.header['DATAMIN'] = np.nanmin(self.hdus[0].data)
        newhdu.header['OBJECT'] = "Point Source Map in Band A"
        newhdu.header['BUNIT'] = "Snowdens (10^-6 counts/s/arcmin^2)"
        newhdu.writeto('./scanpath_FITS/BandA_Point_Sources_skyview.fits',  overwrite=True)
    
    def sort_by_lat(self, LATS, CRs):
        LATS, CRs = zip(*sorted(zip(LATS, CRs)))
        LATS_sorted = []
        CRs_sorted = []
        for i in range(len(LATS)):
            LATS_sorted.append(float(LATS[i]))
            CRs_sorted.append(CRs[i])
        return LATS_sorted, CRs_sorted
    
    def sort_by_long(self, LONGS, CRs):
        LONGS, CRs = zip(*sorted(zip(LONGS, CRs)))
        LONGS_sorted = []
        CRs_sorted = []
        for i in range(len(LONGS)):
            LONGS_sorted.append(float(LONGS[i]))
            CRs_sorted.append(CRs[i])
        return LONGS_sorted, CRs_sorted

    def CR_vs_lat(self):
        LATS = []
        CRs = []
        data = self.hdus[0].data
        for i in range(len(data)):
            for j in range(len(data[i])):
                if ~np.isnan(data[i][j]):
                    x = j+1
                    y = i+1
                    LONG, LAT = self.ws[0].wcs_pix2world(x,y,1)
                    data_point = data[i][j]
                    if LAT not in LATS:
                        LATS.append(LAT)
                        CRs.append(data_point)
                    else:
                        m = LATS.index(LAT)
                        avg_CR = (CRs[m]+data_point)/2
                        CRs[m]= avg_CR
        # LONGS, CRs = self.sort_by_long(LONGS, CRs)
        CRs = [x for _,x in sorted(zip(LATS,CRs))]
        LATS.sort() 
        return LATS, CRs

    def CR_vs_long(self):
        LONGS = []
        CRs = []
        data = self.hdus[0].data
        for i in range(len(data)):
            for j in range(len(data[i])):
                if ~np.isnan(data[i][j]):
                    x = j+1
                    y = i+1
                    LONG, LAT = self.ws[0].wcs_pix2world(x,y,1)
                    data_point = data[i][j]
                    if LONG not in LONGS:
                        LONGS.append(LONG)
                        CRs.append(data_point)
                    else:
                        m = LONGS.index(LONG)
                        avg_CR = (CRs[m]+data_point)/2
                        CRs[m]= avg_CR
        # LONGS, CRs = self.sort_by_long(LONGS, CRs)
        CRs = [x for _,x in sorted(zip(LONGS,CRs))]
        LONGS.sort() 
        return LONGS, CRs

    def interpolate_lat(self):
        LATS, CRs = self.CR_vs_lat()
        x = []
        with open('g_lat_point1_deg.txt', 'r') as lats:
            for LAT in lats:
                x.append(float(LAT))
        interpolated_CRs = np.interp(x,LATS,CRs)
        return x, interpolated_CRs

    def interpolate(self):
        LONGS, CRs = self.CR_vs_long()
        x = []
        with open('g_long5deg', 'r') as longs:
            for LONG in longs:
                x.append(float(LONG))
        interpolated_CRs = np.interp(x,LONGS,CRs)
        return x, interpolated_CRs

    def write_interpolated_CR_vs_lat(self):
        LATS, CRs = self.interpolate_lat() 
        f= open("CR_vs_LAT_R2_transverse_half_loop.txt","w+")
        for i in range(len(LATS)):
            f.write("%f\t%f\n" % (LATS[i], CRs[i]))
        f.close()

    def write_interpolated_CR_vs_long(self):
        LONGS, CRs = self.interpolate() 
        f= open("CR_vs_LONG_boron_5degree.txt","w+")
        for i in range(len(LONGS)):
            f.write("%f\t%f\n" % (LONGS[i], CRs[i]))
        f.close()

    def write_scaled_CR(self, factor):
        scaled_data_array = self.hdus[0].data*factor
        newhdu= fits.PrimaryHDU(scaled_data_array)
        newhdu.header = self.headers[0]
        newhdu.header['OBJECT'] = "two-thirds of Band A PS CR LT 0.025 cps"
        newhdu.header['DATAMAX'] = np.nanmax(scaled_data_array)
        newhdu.header['DATAMIN'] = np.nanmin(scaled_data_array)
        newhdu.writeto('./scanpath_FITS/%s_two_thirds.fits' % self.filenames[0][:-5],  overwrite=True)
        for hdu_list in self.hdu_lists:
            hdu_list.close()

    # def write_coord_wData_toTXT(self):
    #     for row in self.hdus[0].data:
    #         for data in row:
                

if __name__ == '__main__':
    RASSs = counts_maps('/home/user/countsMaps.fits')
    #RASSs.write_interpolated_CR_vs_lat()
    RASSs.zero2Nan()
