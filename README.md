# All Sky Map (FITS) Tools

These tools are relevant to astronomers analyzing sky surveys in FITS formats.

## Finding a weighted average of a sky survey along a path

Using weighted_avg_along_path.py, you can specify the galactic coordinates of a 1-dimensional path and a counts map of some astronomical survey and find a Gaussian weighted average along the provided coordinates. The coordinates are read out of coordinates.dat. You may need to modify the function so that it reads the coordinates of interest.

## General tools to handle one or more FITS counts maps

Some of the operations you can perform include:
- summing maps together
- create a blank map of some template counts map. (all zeroes or NaNs)
- write count rates to a blank map
- Interpolating count rates
